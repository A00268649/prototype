import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
public class CalculateTest {

    private Calculate calculate;
    private CalculationFunctions cf;

    @Before
    public void setUp(){
        calculate = new Calculate(1, 2);
        cf = mock(CalculationFunctions.class);
    }

    @Test
    public void addTest(){
        when(cf.add(1,2)).thenReturn(3);

        assertEquals(calculate.addNumbers(cf), 3);
        verify(cf, times(1)).add(1,2);
    }
    @Test
    public void successTest(){
        assertEquals("Success", calculate.success());
    }

}