public class Calculate {

    private int number1;
    private int number2;

    public Calculate(int number1, int number2){
        this.number1 = number1;
        this.number2 = number2;
    }

    public int addNumbers(CalculationFunctions operations) {
       return operations.add(number1, number2);
    }

    public String success(){
        return "Success";
    }
}
